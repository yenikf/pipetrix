#include "game_core.h"

GameCore::GameCore(QObject * parent, RenderWidget& control, RenderWidget& app, const int height, const int length)
	: QObject(parent)
	, controlView(control), appView(app), startHeight(height), frameLength(length)
{
	timer = new QTimer(this);
	timer->setTimerType(Qt::PreciseTimer);
	timer->setInterval(frameLength);

	connect(timer, &QTimer::timeout, [this] { runFrame(); });
}

bool GameCore::isRunning() const
{
	return  gameRunning;
}

bool GameCore::isPaused() const
{
	return gamePaused;
}

QTimer * const& GameCore::getTimer() const
{
	return timer;
}

Playfield const& GameCore::getBackground() const
{
	return background;
}

std::vector<TetrisShape> const& GameCore::getShapes() const
{
	return shapes;
}

void GameCore::startGame()
{
	gamePaused = false;
	gameRunning = true;

	gameScore = 0;
	gameLines = 0;
	gameHeight = 0;
	frameCount = 0;

	emit scoreChanged(0);
	emit linesChanged(0);
	emit heightChanged(0);
	shapes.clear();
	background.clear();
	appView.resetViewOffset();
	controlView.resetViewOffset();

	shapes.push_back(shapeFactory(startHeight - gameHeight));
	timer->start();
	return;
}

void GameCore::stopGame()
{
	if (isRunning()) {
		gamePaused = false;
		gameRunning = false;
		shapes.clear();
		background.clear();
		timer->stop();
	}
	return;
}

void GameCore::gameOver()
{
	timer->stop();
	emit gameLost(gameScore, gameLines, gameHeight);
	stopGame();
	return;
}

void GameCore::togglePause()
{
	if (isPaused()) {
		timer->start();
		gamePaused = false;
	} else {
		timer->stop();
		gamePaused = true;
	}
	return;
}

void GameCore::moveShapeLeft()
{
	if (isRunning() && !isPaused()) {
		TetrisShape tmp = shapes.back();
		tmp.left();
		if (inView(controlView, tmp) && !background.collidesWith(tmp)) {
			shapes.back() = tmp;
			controlView.repaint();
		}
	}
	return;
}

void GameCore::moveShapeRight()
{
	if (isRunning() && !isPaused()) {
		TetrisShape tmp = shapes.back();
		tmp.right();
		if (inView(controlView, tmp) && !background.collidesWith(tmp)) {
			shapes.back() = tmp;
			controlView.repaint();
		}
	}
	return;
}

void GameCore::rotateShape()
{
	if (isRunning() && !isPaused()) {
		TetrisShape tmp = shapes.back();
		tmp.rotateRight();
		if (inView(controlView, tmp) && !background.collidesWith(tmp)) {
			shapes.back() = tmp;
			controlView.repaint();
		}
	}
	return;
}

void GameCore::adjustScore(int delLines)
{
	switch (delLines) {
		case 4:
			gameScore += 4;
		case 3:
			gameScore += 3;
		case 2:
			gameScore += 2;
		case 1:
			gameScore += 1;
			emit scoreChanged(gameScore);
			break;
	}
	return;
}

void GameCore::adjustLines(int delLines)
{
	int newLines = gameLines + delLines;
	emit linesChanged(newLines);
	if ((gameLines != newLines) && (newLines % 5 <= gameLines % 5)) {		// each 5 deleted lines we get more difficult game
		++gameHeight;
		emit heightChanged(gameHeight);
	}
	gameLines = newLines;
	return;
}

bool GameCore::inView(RenderWidget const& view, TetrisShape const& shape)
{
	auto limits = view.getViewHeightLimits();
	for (auto const& x : shape.getElementsInAbsolute()) {
		if (x.mY >= limits.first && limits.second > x.mY) {
			return true;
		}
	}
	return false;
}

void GameCore::runFrame()
{
	for (auto elem = shapes.begin(); elem != shapes.end(); ) {
		TetrisShape tmp = *elem;
		tmp.drop();
		if (!background.collidesWith(tmp)) {
			*elem = tmp;
			++elem;
		} else {
			std::set<std::vector<Playfield::Line>::size_type> removedLines;
			bool acquired = background.acquireShape(*elem, removedLines);
			if (!acquired) {
				gameOver();
				return;
			}
			elem = shapes.erase(elem);
			int removed_lines_count = background.checkAndReplaceFullLines(removedLines);
			adjustScore(removed_lines_count);
			adjustLines(removed_lines_count);
		}
	}
	if (++frameCount >= 16) {
		shapes.push_back(shapeFactory(startHeight - gameHeight));
		frameCount = 0;
	}
	return;
}
