#pragma once

#include <QMenuBar>
#include <QMenu>
#include <QAction>

/*!
 * The application menu.
 */
class AppMenu : public QMenuBar
{
	Q_OBJECT

public:
	AppMenu(QWidget * parent);
	QMenu * gameMenu;
	QMenu * aboutMenu;
	QAction * gameNew;
	QAction * gameEnd;
	QAction * gamePause;
	QAction * gameQuit;
	QAction * aboutGame;
	QAction * aboutQt;
};
