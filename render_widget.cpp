#include <algorithm>

#include "render_widget.h"

/*!
 * Default ctor. Creates view of size numElemsX & numElemsY at Y coordinate viewOffset.
 */
RenderWidget::RenderWidget(QWidget * parent, QPoint const& pos, int numElemsX, int numElemsY, int viewOffset)
	: QWidget(parent)
	, mViewWidth(numElemsX), mViewHeight(numElemsY), mViewOffsetOriginal(viewOffset)
{
	setAttribute(Qt::WA_PaintOnScreen);			// only works on X11
	setAttribute(Qt::WA_NoSystemBackground);
	move(pos);
	resize(QSize(numElemsX * static_cast<int>(mElementSize), numElemsY * static_cast<int>(mElementSize)));
}

/*!
 * Clears the screen to all black.
 */
void RenderWidget::clearScreen()
{
	this->clear();
	return;
}

/*!
 * Sets the view offset to the newOffset value.
 */
void RenderWidget::setViewOffset(int newOffset)
{
	mViewOffset = newOffset;
	return;
}

/*!
 * Resets the offset of the view to the original value.
 */
void RenderWidget::resetViewOffset()
{
	setViewOffset(0);
	return;
}

/*!
 * Adjusts the offset and thus the position of the view. Use the default value (-1).
 */
void RenderWidget::adjustViewOffset()
{
	++mViewOffset;
	return;
}

/*!
 * Queries for absolute view position (so we can test if the element is still in the view).
 */
std::pair<int, int> RenderWidget::getViewHeightLimits() const
{
	return { mViewOffsetOriginal - mViewOffset, mViewOffsetOriginal - mViewOffset + mViewHeight };
}

/*!
 * Draws a shape by drawing all the elements of a shape.
 */
void RenderWidget::drawShape(TetrisShape const& shape)
{
	auto elems = shape.getElementsInAbsolute();
	auto shape_color = shape.getShapeColor();
	for (auto const& x : elems) {
		this->drawElement(x.mX, x.mY, shape_color);
	}
	return;
}

/*!
 * Draws the playfield. This should be probably optimized later for not redrawing everything each turn.
 */
void RenderWidget::drawPlayfield(Playfield const& playfield)
{
	for (int y = 0; y < mViewHeight; ++y) {
		for (int x = 0; x < mViewWidth; ++x) {
			sf::Color elem_color = playfield.showAt(y, x);
			if (elem_color != sf::Color::Black) {
				this->drawElement(x, y, elem_color);
			}
		}
	}
	return;
}

/*!
 * Acquires object of the Playfield class for rendering.
 * \param p The Playfield object.
 */
void RenderWidget::acquirePlayfield(Playfield const& p)
{
	pPlayfield = &p;
	return;
}

/*!
 * Acquires Container of the TetrisShape shapes for rendering.
 * \param shapes The container.
 */
void RenderWidget::acquireShapeContainer(std::vector<TetrisShape> const& shapes)
{
	pShapes = &shapes;
	return;
}

/*!
 * Draws single element. RectangleShape rec is always the same parameters therefore is made static.
 */
void RenderWidget::drawElement(int x, int y, sf::Color color)
{
	static sf::RectangleShape rec{ sf::Vector2f(mElementSize, mElementSize) };
	rec.setFillColor(color);
	rec.setPosition(mElementSize * x, mElementSize * (y - getViewHeightLimits().first));
	this->draw(rec);
	return;
}

/*!
 * QWidget showEvent method. Gets called when the widget is shown for the first time.
 * \param event Not used here.
 */
void RenderWidget::showEvent(QShowEvent * event)
{
	if (!initialized) {
		// The ID behaves differently on different paltforms.
#ifdef _WIN32
		RenderWindow::create(reinterpret_cast<sf::WindowHandle>(winId()));
#else
		RenderWindow::create(winId());
#endif
		clearScreen();

		initialized = true;
	}
	return;
}

/*!
 * QWidget paintEvent method. Is called when there is a request to draw the widget.
 * We draw the background and all the shapes here.
 * \param event Not used.
 */
void RenderWidget::paintEvent(QPaintEvent * event)
{
	clearScreen();
	if (getViewHeightLimits().second >= 0) {
		drawPlayfield(* pPlayfield);
	}
	std::for_each(pShapes->cbegin(), pShapes->cend(), [this](TetrisShape const& s) { drawShape(s); });

	display();
	return;
}

/*!
 * QWidget paintEngine returns the widget's paint engine. We paint on our own so we return nullptr.
 * \return nullptr as there is no paint engine.
 */
QPaintEngine * RenderWidget::paintEngine() const
{
	return nullptr;
}

/*!
 * The default size of a single element any shape consists of.
 * Note that the main window has size hardwired and will need to change accordingly when this value changes.
 */
const float RenderWidget::mElementSize = 25.f;
