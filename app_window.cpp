#include "app_window.h"

#include <QApplication>
#include <QMessageBox>

AppWindow::AppWindow()
{
	setWindowTitle("Pipetrix Qt5");
	setFixedSize(270, 880);

	controlView = new RenderWidget(this, QPoint(10, 30), 10, 12, startHeight);
	appView = new RenderWidget(this, QPoint(10, 340), 10, 20, 0);
	menu = new AppMenu(this);
	setMenuBar(menu);
	scoreBar = new AppBar(this);
	setStatusBar(scoreBar);
	pipetrix = new GameCore(this, *controlView, *appView, startHeight);

	controlView->acquirePlayfield(pipetrix->getBackground());
	controlView->acquireShapeContainer(pipetrix->getShapes());
	appView->acquirePlayfield(pipetrix->getBackground());
	appView->acquireShapeContainer(pipetrix->getShapes());

	controlView->show();
	appView->show();
	menu->show();
	scoreBar->show();
	this->show();

	connect(menu->gameNew, &QAction::triggered, this, &AppWindow::startGame);
	connect(menu->gameEnd, &QAction::triggered, this, &AppWindow::stopGame);
	connect(menu->gamePause, &QAction::triggered, this, &AppWindow::pauseGame);
	connect(menu->gameQuit, &QAction::triggered, this, &AppWindow::close);
	connect(menu->aboutGame, &QAction::triggered, this, &AppWindow::showAboutDialog);
	connect(menu->aboutQt, &QAction::triggered, this, &QApplication::aboutQt);

	connect(pipetrix, &GameCore::gameLost, this, &AppWindow::showGameOverDialog);
	connect(pipetrix, &GameCore::scoreChanged, scoreBar, &AppBar::updateScore);
	connect(pipetrix, &GameCore::linesChanged, scoreBar, &AppBar::updateLines);
	connect(pipetrix, &GameCore::heightChanged, scoreBar, &AppBar::updateHeight);
	connect(pipetrix, &GameCore::heightChanged, controlView, &RenderWidget::setViewOffset);

	connect(pipetrix->getTimer(), &QTimer::timeout, [this] { controlView->repaint(); } );
	connect(pipetrix->getTimer(), &QTimer::timeout, [this] { appView->repaint(); } );
}

/*!
 * The QWidget keyPressEvent method. Overridden to handle keys we care for.
 * \param event The keypress event.
 */
void AppWindow::keyPressEvent(QKeyEvent * event)
{
	switch (event->key()) {
		case Qt::Key_Left:
			pipetrix->moveShapeLeft();
			break;
		case Qt::Key_Right:
			pipetrix->moveShapeRight();
			break;
		case Qt::Key_Up:
			pipetrix->rotateShape();
			break;
	}
	return;
}

void AppWindow::startGame()
{
	pipetrix->startGame();
	return;
}

void AppWindow::stopGame()
{
	pipetrix->stopGame();
	return;
}

void AppWindow::pauseGame()
{
	if (pipetrix->isRunning()) {
		pipetrix->togglePause();
	}
	return;
}

/*!
 * Shows the about dialog. Not finished yet.
 */
void AppWindow::showAboutDialog()
{
	QMessageBox::about(this, "About Pipetrix", "Pipetrix version 1.0.4<br><br>Copyright © 2019-2020 Jan Feifič (yenikf@outlook.com)<br><br>"
											   "<a href=\"https://bitbucket.org/yenikf/pipetrix/src/master/\">Source code</a> <br><br>Developed using Qt and SFML.");
	return;
}

/*!
 * Shows the game over dialog
 */
void AppWindow::showGameOverDialog(int score, int lines, int height)
{
	QMessageBox::about(this, "Game Over!", QStringLiteral("Oh no! You lost the game!\n\nYou managed to achieve a score of: %1\n"
														  "by destroying %2 lines, thus raising the height to %3 lines above standard.").arg(score).arg(lines).arg(height));
	return;
}
