#include "element.h"

//! Constructor
Element::Element(int x, int y)
	: mX{ x }, mY{ y }
{}

/*!
 * Rotate the element around the 0,0 coordinate, counterclockwise.
 */
void Element::rotateLeft()
{
	int tmp = mX;
	mX = mY;
	mY = -tmp;
	return;
}

/*!
 * Rotate the element around the 0,0 coordinate, clockwise.
 */
void Element::rotateRight()
{
	int tmp = mX;
	mX = -mY;
	mY = tmp;
	return;
}
