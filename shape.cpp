#include <ctime>
#include <random>

#include "shape.h"

//! Constructor
TetrisShape::TetrisShape(ShapeElements const& elements, sf::Color const& color, int startAbsY)
	: mElements{ elements }, mColor{ color }, mAbsY{ startAbsY }
{}

/*!
 * Get absolute coordinates of the Elements a TetrisShape consists of.
 * For colision detection only! Do not use it to create new TetrisShape, the coordinates will be way off.
 */
ShapeElements TetrisShape::getElementsInAbsolute() const
{
	ShapeElements shapeCopy{ mElements };
	for (auto& x : shapeCopy) {
		x.mX += mAbsX;
		x.mY += mAbsY;
	}
	return shapeCopy;
}

/*!
 * Returns color of a TetrisShape.
 */
sf::Color TetrisShape::getShapeColor() const
{
	return mColor;
}

/*!
 * Rotate the shape by rotating the elements, counterclockwise.
 */
void TetrisShape::rotateLeft()
{
	for (auto& x : mElements) {
		x.rotateLeft();
	}
	return;
}

/*!
 * Rotate the shape by rotating the elements, clockwise.
 */
void TetrisShape::rotateRight()
{
	for (auto& x : mElements) {
		x.rotateRight();
	}
	return;
}

/*!
 * Move the shape left one step.
 */
void TetrisShape::left()
{
	--mAbsX;
	return;
}

/*!
 * Move the shape right one step.
 */
void TetrisShape::right()
{
	++mAbsX;
	return;
}

/*!
 * Descend the shape by one step.
 */
void TetrisShape::drop()
{
	++mAbsY;
	return;
}

/*!
 * Generate random TetrisShape.
 */
TetrisShape shapeFactory(int startY)
{
	static std::random_device seeder;
	static const auto seed = seeder.entropy() ? seeder() : time(nullptr);
	static std::default_random_engine e(static_cast<unsigned>(seed));
	static std::uniform_int_distribution<int> distrib(0, 6);

	return TetrisShape(getRandomShape(distrib(e)), getRandomColor(distrib(e)), startY);
}

/*!
 * Randomly select one of the ShapeElements configurations.
 */
ShapeElements getRandomShape(int rnd)
{
	static std::array<ShapeElements, 7> shapes{ {
		{ Element(0,1), Element(1,0), Element(1,1), Element(0,0) },		// o-shape
		{ Element(0,1), Element(1,0), Element(1,-1), Element(0,0) },	// s-shape
		{ Element(0,-1), Element(1,0), Element(1,1), Element(0,0) },	// z-shape
		{ Element(0,-1), Element(0,1), Element(1,1), Element(0,0) },	// j-shape
		{ Element(0,-1), Element(0,1), Element(-1,1), Element(0,0) },	// l-shape
		{ Element(1,0), Element (-1,0), Element(0,-1), Element(0,0) },	// t-shape
		{ Element(0,1), Element(0,2), Element(0,-1), Element(0,0) },	// i-shape
	} };										// brace elision shenanigans

	return shapes[rnd];
}

/*!
 * Select random color for new TetrisShape.
 */
sf::Color getRandomColor(int rnd)
{
	static std::array<sf::Color, 7> colors{
		sf::Color::White,
		sf::Color::Red,
		sf::Color::Green,
		sf::Color::Blue,
		sf::Color::Yellow,
		sf::Color::Magenta,
		sf::Color::Cyan
	};

	return colors[rnd];
}
