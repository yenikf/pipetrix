#include "app_menu.h"

AppMenu::AppMenu(QWidget * parent)
	: QMenuBar(parent)
{
	gameNew = new QAction("&New", this);
	gameNew->setShortcut(QKeySequence("Ctrl+N"));
	gameEnd = new QAction("&End", this);
	gameEnd->setShortcut(QKeySequence("Ctrl+E"));
	gamePause = new QAction("&Pause", this);
	gamePause->setShortcut(QKeySequence("Ctrl+P"));
	gameQuit = new QAction("&Quit", this);
	gameQuit->setShortcut(QKeySequence("Ctrl+Q"));

	gameMenu = new QMenu("&Game", this);
	gameMenu->addAction(gameNew);
	gameMenu->addAction(gamePause);
	gameMenu->addAction(gameEnd);
	gameMenu->addSeparator();
	gameMenu->addAction(gameQuit);

	aboutGame = new QAction("About &Pipetrix", this);
	aboutQt = new QAction("About &Qt", this);

	aboutMenu = new QMenu("&About", this);
	aboutMenu->addAction(aboutGame);
	aboutMenu->addAction(aboutQt);

	this->addMenu(gameMenu);
	this->addMenu(aboutMenu);
}
