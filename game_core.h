#pragma once

#include <vector>
#include <QObject>
#include <QTimer>

#include "shape.h"
#include "playfield.h"
#include "render_widget.h"

/*!
 * Class containing game logic.
 */
class GameCore : public QObject
{
	Q_OBJECT;

public:
	GameCore(QObject * parent, RenderWidget& control, RenderWidget& app, const int height, const int length = 320);
	bool isRunning() const;
	bool isPaused() const;
	QTimer * const & getTimer() const;
	Playfield const& getBackground() const;
	std::vector<TetrisShape> const& getShapes() const;
	void startGame();
	void stopGame();
	void gameOver();
	void togglePause();
	void moveShapeLeft();
	void moveShapeRight();
	void rotateShape();

signals:
	void scoreChanged(int newScore);
	void linesChanged(int newLines);
	void heightChanged(int newHeight);
	void gameLost(int score, int lines, int height);

private:
	void adjustScore(int delLines);
	void adjustLines(int delLines);
	bool inView(RenderWidget const& view, TetrisShape const& shape);
	void runFrame();

	RenderWidget& controlView;
	RenderWidget& appView;
	QTimer * timer;

	bool gameRunning{};
	bool gamePaused{};

	const int startHeight;
	const int frameLength;

	std::vector<TetrisShape> shapes;
	Playfield background;
	int gameScore{};
	int gameLines{};
	int gameHeight{};
	int frameCount{};
};
