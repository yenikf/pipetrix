#pragma once

#include <array>
#include <SFML/Graphics/Color.hpp>

#include "element.h"

//! Typedef for the Element container
using ShapeElements = std::array<Element, 4>;

/*!
 * Tetris shape class.
 */
class TetrisShape
{
public:

	TetrisShape(ShapeElements const&, sf::Color const&, int);
	
	ShapeElements getElementsInAbsolute() const;
	sf::Color getShapeColor() const;

	void rotateLeft();
	void rotateRight();
	void left();
	void right();
	void drop();
private:
	ShapeElements mElements{};				//! Elements the TetrisShape consists of
	sf::Color mColor{};						//! Color of the TetrisShape
	int mAbsX{ 4 };							//! Absolute coordinates
	int mAbsY{ -20 };
};

TetrisShape shapeFactory(int);				//! create new TetrisShape
ShapeElements getRandomShape(int);			//! generate random ShapeElements for TetrisShape
sf::Color getRandomColor(int);				//! generate random color for TetrisShape
