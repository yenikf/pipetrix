#pragma once

#include <utility>
#include <QWidget>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "shape.h"
#include "playfield.h"

class RenderWidget : public QWidget, public sf::RenderWindow
{
	Q_OBJECT

public:
	RenderWidget(QWidget * parent, QPoint const& pos, int numElemsX, int numElemsY, int viewOffset);

	void clearScreen();
	void setViewOffset(int newOffset);
	void resetViewOffset();
	void adjustViewOffset();
	std::pair<int, int> getViewHeightLimits() const;
	void drawShape(TetrisShape const& shape);
	void drawPlayfield(Playfield const& playfield);
	void acquirePlayfield(Playfield const& p);
	void acquireShapeContainer(std::vector<TetrisShape> const& shapes);

private:
	void drawElement(int x, int y , sf::Color color);

	virtual void showEvent(QShowEvent *) override;
	virtual void paintEvent(QPaintEvent *) override;
	virtual QPaintEngine * paintEngine() const override;

	bool initialized{};

	static const float mElementSize;
	int mViewWidth{};
	int mViewHeight{};
	int mViewOffset{};
	const int mViewOffsetOriginal;

	Playfield const * pPlayfield;
	std::vector<TetrisShape> const * pShapes;
};
