#include <QString>

#include "app_bar.h"

AppBar::AppBar(QWidget * parent)
	: QStatusBar(parent)
{
	scoreLabel = new QLabel();
	linesLabel = new QLabel();
	heightLabel = new QLabel();
	addWidget(scoreLabel, 1);
	addWidget(linesLabel, 1);
	addWidget(heightLabel, 1);
}

/*!
 * Changes the score count and updates the statusbar.
 * \param score The score to show.
 */
void AppBar::updateScore(int score)
{
	scoreLabel->setText(QStringLiteral("Score: %1").arg(score));
	return;
}

/*!
 * Changes the line count and updates the statusbar.
 * \param lines The line count.
 */
void AppBar::updateLines(int lines)
{
	linesLabel->setText(QStringLiteral("Lines: %1").arg(lines));
	return;
}

/*!
 * Changes the height and updates the statusbar.
 * \param height The height between the two views.
 */
void AppBar::updateHeight(int height)
{
	heightLabel->setText(QStringLiteral("Height: %1").arg(height));
	return;
}

/*!
 * Rewrites the statusbar message with new one.
 */
void AppBar::update()
{
	clearMessage();
	return;
}
