#pragma once

/*!
 * Single Tetris piece element. Each Tetris piece consists of bunch of these.
 */
struct Element
{
	Element() = default;			//!< This is the same as Element(0,0) ctor
	Element(int, int);
	void rotateLeft();
	void rotateRight();

	int mX{};
	int mY{};
};
