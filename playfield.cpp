#include <algorithm>

#include "playfield.h"

/*!
 * Default ctor. Creates 20 rows of black colored Lines.
 */
Playfield::Playfield()
{
	mField.reserve(20);
	for (size_t i = 0; i < 20; ++i) {
		mField.push_back(getEmptyLine());
	}
}

/*!
 * Erases the playfield. (Sets the color of all squares to black.)
 */
void Playfield::clear()
{
	for (Line& line : mField) {
		for (sf::Color& color : line) {
			color = sf::Color::Black;
		}
	}
	return;
}

/*!
 * Check Shape for collision. Each Element of a Shape is tested for being inside the playfield and not colliding with existing piece.
 */
bool Playfield::collidesWith(TetrisShape const& shape) const
{
	ShapeElements elems = shape.getElementsInAbsolute();
	for (auto const& x : elems) {
		if (isOutOfBorders(x) || elementCollides(x)) {
			return true;
		}
	}
	return false;
}

/*!
 * If a collision occured, the previous iteration of a Shape is added to the playfield,
 * by acquiring the Element's position and the Shape color.
 * \param removedLines Indices of Lines elements were added to (we need to check those for full). We want to know which lines get changed
 * \return True if the shape elements was succesfully transferred into playfield.
 */
bool Playfield::acquireShape(TetrisShape const& shape, std::set<std::vector<Playfield::Line>::size_type>& removedLines)
{
	ShapeElements elems = shape.getElementsInAbsolute();
	sf::Color shape_color = shape.getShapeColor();
	for (auto const& x : elems) {
		if (x.mY < 0) {							//!< The element is over the upper boundary of our 10x20 field
			return false;
		}
		mField[x.mY][x.mX] = shape_color;
		removedLines.insert(x.mY);
	}
	return true;
}

/*!
 * Check lines if they're full, and erase them if needed.
 * \return number of lines erased (for score count etc.)
 */
int Playfield::checkAndReplaceFullLines(std::set<std::vector<Line>::size_type> const& v)
{
	int numFullLines{ 0 };
	for (auto x : v) {
		if (lineFull(mField[x])) {
			replaceLineAt(x);
			++numFullLines;
		}
	}
	return numFullLines;
}

/*!
 * Get the color value of a Playfield element. Black color means empty element.
 */
sf::Color Playfield::showAt(std::vector<Line>::size_type yCoord, Line::size_type xCoord) const
{
	return mField[yCoord][xCoord];
}

/*!
 * Check for Playfield border overrun. If the x is off the 10x20 grid on any side or bottom, it collides.
 */
bool Playfield::isOutOfBorders(Element const& x) const
{
	if (mLeftBorder >= x.mX || x.mX >= mRightBorder || mBottomBorder <= x.mY) {
		return true;
	}
	return false;
}

/*!
 * Check Element for collision. If the mField at x's coordinates is not empty, the x collides.
 * The x.mY >= 0 condition prevents overruns -- if the Element is over the upper boundary, it cannnot collide.
 */
bool Playfield::elementCollides(Element const& x) const
{
	if (x.mY >= 0 && mField[x.mY][x.mX] != sf::Color::Black) {
		return true;
	}
	return false;
}

/*!
 * Check if the Line is full (none of the elements is empty).
 */
bool Playfield::lineFull(Line const& line) const
{
	return std::none_of(cbegin(line), cend(line), [](sf::Color const& elem) { return elem == sf::Color::Black; });
}

/*!
 * Erase the Line at index and create new empty one at the top.
 */
void Playfield::replaceLineAt(std::vector<Line>::size_type index)
{
	mField.erase(begin(mField) + index);			// remove full Line
	mField.insert(cbegin(mField), getEmptyLine());	// default-initialize empty Line at the top
	return;
}

/*!
 * Sets the color of all elements in a line to black
 */
Playfield::Line Playfield::getEmptyLine()
{
	Line l;
	for (auto& x : l) {
		x = sf::Color::Black;
	}
	return l;
}
