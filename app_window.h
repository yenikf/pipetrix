#pragma once

#include <QMainWindow>
#include <QKeyEvent>

#include "render_widget.h"
#include "app_menu.h"
#include "app_bar.h"
#include "game_core.h"

/*!
 * The main application window widget.
 */
class AppWindow : public QMainWindow
{
	Q_OBJECT

public:
	AppWindow();
	void keyPressEvent(QKeyEvent * event) override;

private slots:
	void startGame();
	void stopGame();
	void pauseGame();
	void showAboutDialog();
	void showGameOverDialog(int score, int lines, int height);

private:
	RenderWidget * controlView;
	RenderWidget * appView;
	AppMenu * menu;
	AppBar * scoreBar;
	GameCore * pipetrix;

	const int startHeight = -18;
};
