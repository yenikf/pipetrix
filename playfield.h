#pragma once

#include <set>
#include <vector>
#include <array>

#include "shape.h"

/*!
 * The 10x20 Tetris playfield.
 * Each line is a vector element, so we can easily erase whichever one (the full one) and create new, and use indices to access each line.
 * Each element is checked for presence by its color, with the black being empty (black is 255 (0x000F) in SFML)
 */
class Playfield {
public:
	using Line = std::array<sf::Color, 10>;

	Playfield();

	void clear();
	bool collidesWith(TetrisShape const&) const;
	bool acquireShape(TetrisShape const&, std::set<std::vector<Playfield::Line>::size_type>&);	//! return value to check for full lines
	int checkAndReplaceFullLines(std::set<std::vector<Line>::size_type> const&);
	sf::Color showAt(std::vector<Line>::size_type, Line::size_type) const;
private:
	bool lineFull(Line const&) const;
	void replaceLineAt(std::vector<Line>::size_type);
	bool isOutOfBorders(Element const&) const;
	bool elementCollides(Element const&) const;
	Line getEmptyLine();

	std::vector<Line> mField;			//! This is set up in the constructor;
	int mLeftBorder{ -1 };
	int mRightBorder{ 10 };
	int mBottomBorder{ 20 };
};
