#pragma once

#include <QStatusBar>
#include <QLabel>

/*!
 * The application statusbar. Show the score and the number of lined deleted.
 * Changes are conveyed by calling the class' slots.
 */
class AppBar : public QStatusBar
{
	Q_OBJECT

public:
	AppBar(QWidget * parent);

public slots:
	void updateScore(int score);
	void updateLines(int lines);
	void updateHeight(int height);

private:
	void update();

	QLabel * scoreLabel;
	QLabel * linesLabel;
	QLabel * heightLabel;
};
