#include <QApplication>

#include "app_window.h"

int main(int argc, char ** argv)
{
	QApplication app(argc, argv);

	AppWindow * window = new AppWindow;
	window->show();

	return app.exec();
}
